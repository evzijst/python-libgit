from collections import Mapping
from functools import wraps

try:
    xrange
except NameError:
    xrange = range

from cffi import FFI

ffi = FFI()
ffi.cdef("""
    /* git2/threads.h */
    int git_threads_init(void);

    /* git2/errors.h */
    typedef struct {
        char *message;
        int klass;
    } git_error;

    const git_error *giterr_last(void);
    void giterr_clear(void);

    /* git2/types.h */
    typedef enum {
        GIT_OBJ_ANY = -2,
        GIT_OBJ_BAD = -1,
        GIT_OBJ__EXT1 = 0,
        GIT_OBJ_COMMIT = 1,
        GIT_OBJ_TREE = 2,
        GIT_OBJ_BLOB = 3,
        GIT_OBJ_TAG = 4,
        GIT_OBJ__EXT2 = 5,
        GIT_OBJ_OFS_DELTA = 6,
        GIT_OBJ_REFS_DELTA = 7,
    } git_otype;

    typedef struct git_odb git_odb;
    typedef struct git_odb_object git_odb_object;
    typedef struct git_repository git_repository;
    typedef struct git_reference git_reference;

    /* git2/oid.h */
    typedef struct git_oid {
        unsigned char id[20];
    } git_oid;

    int git_oid_fromstrn(git_oid *out, const char *str, size_t length);

    /* git2/odb.h */
    void git_odb_free(git_odb *db);
    int git_odb_read_prefix(git_odb_object **out, git_odb *db,
                            const git_oid *short_id, size_t len);

    void git_odb_object_free(git_odb_object *object);
    const git_oid *git_odb_object_id(git_odb_object *object);
    size_t git_odb_object_size(git_odb_object *object);
    git_otype git_odb_object_type(git_odb_object *object);

    /* git2/repository.h */
    typedef enum {
        GIT_REPOSITORY_OPEN_NO_SEARCH = 1,
        GIT_REPOSITORY_OPEN_CROSS_FS = 2,
    } git_repository_open_flag_t;

    int git_repository_open_ext(git_repository **out, const char *path,
                                unsigned int flags, const char *ceiling_dirs);
    void git_repository_free(git_repository *repo);
    int git_repository_odb(git_odb **out, git_repository *repo);

    /* git2/strarray.h */
    typedef struct git_strarray {
        char **strings;
        size_t count;
    } git_strarray;

    void git_strarray_free(git_strarray *array);

    /* git2/refs.h */
    typedef enum {
        GIT_REF_INVALID = 0,
        GIT_REF_OID = 1,
        GIT_REF_SYMBOLIC = 2,
        GIT_REF_LISTALL = 3
    } git_ref_t;

    int git_reference_lookup(git_reference **out, git_repository *repo,
                             const char *name);
    const git_oid *git_reference_target(const git_reference *ref);
    const char *git_reference_symbolic_target(const git_reference *ref);
    const char *git_reference_name(const git_reference *ref);
    int git_reference_resolve(git_reference **out, const git_reference *ref);
    int git_reference_list(git_strarray *array, git_repository *repo,
                           unsigned int list_flags);
    void git_reference_free(git_reference *ref);
    int git_reference_cmp(git_reference *ref1, git_reference *ref2);

    /* git2/object.h */
    const char *git_object_type2string(git_otype type);
         """)
libgit2 = ffi.dlopen('libgit2.0.18.0')
libgit2.git_threads_init()

giterr_last = libgit2.giterr_last
giterr_clear = libgit2.giterr_clear

class GitError(Exception):
    pass

def handleerror(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        ret = func(*args, **kwargs)
        if ret:
            err = giterr_last()
            try:
                raise GitError(ffi.string(err.message))
            finally:
                giterr_clear()
        return ret
    return wrapper

GIT_OID_MINPREFIXLEN = 4
git_oid_fromstrn = handleerror(libgit2.git_oid_fromstrn)

git_odb_free = libgit2.git_odb_free
git_odb_read_prefix = handleerror(libgit2.git_odb_read_prefix)

git_odb_object_free = libgit2.git_odb_object_free
git_odb_object_id = libgit2.git_odb_object_id
git_odb_object_size = libgit2.git_odb_object_size
git_odb_object_type = libgit2.git_odb_object_type

GIT_REPOSITORY_OPEN_NO_SEARCH = libgit2.GIT_REPOSITORY_OPEN_NO_SEARCH
git_repository_open_ext = handleerror(libgit2.git_repository_open_ext)
git_repository_free = libgit2.git_repository_free
git_repository_odb = handleerror(libgit2.git_repository_odb)

git_strarray_free = libgit2.git_strarray_free

GIT_REF_LISTALL = libgit2.GIT_REF_LISTALL
git_reference_lookup = handleerror(libgit2.git_reference_lookup)
git_reference_target = libgit2.git_reference_target
git_reference_symbolic_target = libgit2.git_reference_symbolic_target
git_reference_name = libgit2.git_reference_name
git_reference_resolve = handleerror(libgit2.git_reference_resolve)
git_reference_list = handleerror(libgit2.git_reference_list)
git_reference_free = libgit2.git_reference_free
git_reference_cmp = libgit2.git_reference_cmp

git_object_type2string = libgit2.git_object_type2string

class Object(object):
    _free = git_odb_object_free

    def __init__(self, obj):
        self._obj = obj

    def __del__(self):
        self._free(self._obj)

    def size(self):
        return git_odb_object_size(self._obj)

    def id(self):
        return git_odb_object_id(self._obj)

    def __eq__(self, other):
        if not isinstance(other, Object):
            return False
        return self.id() == other.id()

    def type(self):
        otype = git_odb_object_type(self._obj)
        return ffi.string(git_object_type2string(otype))

class Reference(object):
    _free = git_reference_free

    def __init__(self, ref):
        self._ref = ref

    def name(self):
        return ffi.string(git_reference_name(self._ref))

    def __del__(self):
        self._free(self._ref)

    def __eq__(self, other):
        if not isinstance(other, Reference):
            return False
        return git_reference_cmp(self._ref, other._ref) == 0

    def target(self):
        oid = git_reference_target(self._ref)
        if oid == ffi.NULL:
            return None
        return ffi.string(oid.id)

    def symtarget(self):
        name = git_reference_symbolic_target(self._ref)
        if name == ffi.NULL:
            return None
        return ffi.string(name)

    def resolve(self):
        outref = ffi.new('git_reference **')
        git_reference_resolve(outref, self._ref)
        ref = Reference(ref=outref[0])
        if ref == self:
            return self
        else:
            return ref

class ReferenceMap(Mapping):
    def __init__(self, repo, names):
        self._repo = repo
        self._names = names

    def keys(self):
        return self._names

    def __iter__(self):
        return iter(self._names)

    def __len__(self):
        return len(self._names)

    def __getitem__(self, name):
        outref = ffi.new('git_reference **')
        git_reference_lookup(outref, self._repo, name)
        return Reference(outref[0])

class Repository(object):
    _free = git_repository_free
    _freeodb = git_odb_free

    def __init__(self, path):
        self._repo = None
        self._odb = None
        outrepo = ffi.new('git_repository **')
        git_repository_open_ext(outrepo, path, GIT_REPOSITORY_OPEN_NO_SEARCH,
                                ffi.NULL)
        self._repo = outrepo[0]
        outodb = ffi.new('git_odb **')
        git_repository_odb(outodb, self._repo)
        self._odb = outodb[0]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __del__(self):
        self.close()

    def close(self):
        if self._repo is not None:
            self._free(self._repo)
            self._repo = None
        if self._odb is not None:
            self._freeodb(self._odb)
            self._odb = None

    def _lookupoidprefix(self, oid, prefixlen):
        outobj = ffi.new('git_odb_object **')
        git_odb_read_prefix(outobj, self._odb, oid, prefixlen)
        return Object(outobj[0])

    def __getitem__(self, key):
        oid = ffi.new('git_oid *')
        git_oid_fromstrn(oid, key, len(key))
        return self._lookupoidprefix(oid, len(key))

    def refs(self):
        reflist = ffi.new('git_strarray *')
        try:
            git_reference_list(reflist, self._repo, GIT_REF_LISTALL)
            names = [ffi.string(reflist.strings[i])
                     for i in xrange(reflist.count)]
        finally:
            git_strarray_free(reflist)
        return ReferenceMap(self._repo, names)

def openrepo(path):
    return Repository(path)
